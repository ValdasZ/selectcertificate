﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiKiller
{
    class Program
    {
        static void killProcess()
        {
            foreach (Process p in Process.GetProcessesByName("NevdaApi"))
            {
                try
                {   
                    p.Kill();
                    p.WaitForExit();
                    
                }
                catch 
                {   
                }
                
            }
        }

        static void Main(string[] args)
        {
            killProcess();
        }
    }
}
