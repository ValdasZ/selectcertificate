﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RunApp
{
    class Program
    {
        static string adminUserName = @"miskas\mkadm";
        static string adminPassword = @"Test1234";


        static void AddFirewallRule(string machineName)
        {
            var args = string.Format(@"\\{0} netsh advfirewall firewall set rule name=""Remote Service Management(RPC)"" profile=domain new enable=yes", machineName);

            //Console.WriteLine(args);

            RunProc("PsExec.exe", args);
        }

        static void RunProc(string fileName, string args, bool showConsole = false)
        {
            ProcessStartInfo psi = new ProcessStartInfo(fileName, args);
            //psi.RedirectStandardOutput = true;
            psi.UseShellExecute = true;

            if (!showConsole)
            {
                psi.CreateNoWindow = true;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
            }

            psi.Verb = "runas";


            var proc = new Process
            {
                StartInfo = psi,                
            };

            proc.Start();


            //while (!proc.StandardOutput.EndOfStream)
            //{
            //    string line = proc.StandardOutput.ReadLine();
            //    Console.WriteLine(line);
            //}
        }

        static void OpenPaint(string machineName)
        {
            string args = string.Format(@"\\{0} -u {1} -p {2} -i mspaint", machineName, adminUserName, adminPassword);
            //Console.WriteLine(args);
            RunProc("PsExec.exe", args);
        }

        static void TurnOffDisplay(string machineName)
        {
            //AddFirewallRule(machineName);

            string args = string.Format(@"\\{0} -u {1} -p {2} -h c:\logs\off.exe", machineName, adminUserName, adminPassword);            
            RunProc("PsExec.exe", args, true);
        }

        static void Main(string[] args)
        {
            //Task.Run(() => Go("ignasb"));
            //Task.Run(() => Go("mariaus"));
            //Task.Run(() => Go("juliaus"));
            //Task.Run(() => Go("aurimo"));

            TurnOffDisplay("ignasb");
            //TurnOffDisplay("mariaus");


            Console.ReadLine();
        }

        static void Go(string machineName)
        {
            AddFirewallRule(machineName);
            //Thread.Sleep(20000);

            for (int i = 0; i < 10; i++)
            {
                OpenPaint(machineName);

            }
        }
    }
}
