﻿using Security.Cryptography;
using Security.Cryptography.X509Certificates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Api
{
    public class AllCertificateStore : AbstractCertificateStore
    {
        protected override X509Certificate2Collection getCollection()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;

            store.Close();

            return collection;
        }
    }
}
