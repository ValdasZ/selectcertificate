﻿using NevdaApi.Models;
using Security.Cryptography;
using Security.Cryptography.X509Certificates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Api
{
    public abstract class AbstractCertificateStore
    {
        public string Sign(SignModel model)
        {
            var cert = getCert(Convert.FromBase64String(model.certificate));

            var algorithm = new CngAlgorithm(model.algorithm);

            string signedHashStr = null;

            if (cert.HasCngKey())
            {
                CngKey key = cert.GetCngPrivateKey();
                var rsaKey = new RSACng(key);

                Console.WriteLine("CNG SignatureAlgorithm: {0}", key.Algorithm);
                Console.WriteLine("CNG PrivateKey Provider: {0}", rsaKey.Provider.Provider);

                var signed = rsaKey.SignData(Convert.FromBase64String(model.dtbs));
                signedHashStr = Convert.ToBase64String(signed);
            }
            else
            {
                RSACryptoServiceProvider privateKeyProvider = (RSACryptoServiceProvider)cert.PrivateKey;
                var alg = privateKeyProvider.SignatureAlgorithm;

                if (privateKeyProvider.CspKeyContainerInfo != null)
                    Console.WriteLine("ProviderName: {0}", privateKeyProvider.CspKeyContainerInfo.ProviderName);

                Console.WriteLine("SignatureAlgorithm: {0}", alg);

                byte[] sig = privateKeyProvider.SignData(Convert.FromBase64String(model.dtbs), CryptoConfig.MapNameToOID(model.algorithm));
                signedHashStr = Convert.ToBase64String(sig);
            }

            return signedHashStr;
        }

        public X509Certificate2 SelectCertificate(bool autoChooseIfOnlyOneCertificate = false)
        {
            X509Certificate2Collection cole = getCollection();

            if (cole.Count == 1 && autoChooseIfOnlyOneCertificate)
                return cole[0];

            X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(cole, "Dokumento pasirašymas", "Pasirinkite sertifikatą kurį norėsite naudoti", X509SelectionFlag.SingleSelection);

            if (scollection != null && scollection.Count > 0)
                return scollection[0];
            else
                return null;
        }

        X509Certificate2 getCert(byte[] certRawData)
        {
            X509Certificate2Collection cole = getCollection();

            if (cole.Count == 1)
                return cole[0];
            else
            {
                var list = cole.ToList();

                foreach (var item in list)
                {
                    if (item.GetRawCertData().SequenceEqual(certRawData))
                        return item;
                }
            }

            throw new Exception("Neranda pasirašymui skirto sertifikato");
        }

        protected abstract X509Certificate2Collection getCollection();

        bool supportSha256(X509Certificate2 cert)
        {
            bool result = false;

            if (cert.HasCngKey())
                result = true;
            else
            {
                try
                {
                    if (cert.PrivateKey != null)
                    {
                        var provider = ((RSACryptoServiceProvider)cert.PrivateKey).CspKeyContainerInfo.ProviderName;
                        result = provider.Trim().Replace(" ", "").Equals("microsoftenhancedrsaandaescryptographicprovider", StringComparison.InvariantCultureIgnoreCase);
                    }
                }
                catch { }
            }


            return result;
        }

        public GetCertificateModel GetCertModel()
        {
            var cert = SelectCertificate();

            if (cert != null)
            {
                var key = Convert.ToBase64String(cert.GetRawCertData());

                return new GetCertificateModel
                {
                    Certificate = key,
                    Algorithm = supportSha256(cert) ? "sha256" : "sha1"
                };
            }

            return null;
        }
    }
}
