﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public static class CertHelper
    {
        public static IList<X509Certificate2> ToList(this X509Certificate2Collection fcollection)
        {
            var items = fcollection.GetEnumerator();
            IList<X509Certificate2> list = new List<X509Certificate2>();
            while (items.MoveNext())
            {
                list.Add(items.Current);
            }

            return list;
        }
    }
}
