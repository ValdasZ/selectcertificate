﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.DirectoryServices;
using System.Collections.Specialized;
using System.Diagnostics;
using Security.Cryptography.X509Certificates;
using Security.Cryptography;


namespace Api
{
    public class WindowsCertificateStore : AbstractCertificateStore
    {
        //protected override X509Certificate2Collection getCollection()
        //{
        //    X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
        //    store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

        //    X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
        //    X509Certificate2Collection fcollectionValidDate = collection.Find(X509FindType.FindByTimeValid, DateTime.Now, true);
        //    X509Certificate2Collection fcollection = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "DigitalSignature", true);
        //    X509Certificate2Collection fcollection2 = fcollection.Find(X509FindType.FindByKeyUsage, "KeyEncipherment", true);
        //    X509Certificate2Collection fcollection3 = fcollection2.Find(X509FindType.FindByIssuerName, "miskas-AZUOLAS-CA", true);

        //    store.Close();

        //    return fcollection3;
        //}

        protected override X509Certificate2Collection getCollection()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.OpenExistingOnly);
            X509Certificate2Collection certificates = store.Certificates;

            //sitos 3 eilutes mano, visa kita paimta is seno xbap adoc
            X509Certificate2Collection fcollectionValidDate = certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, true);
            X509Certificate2Collection fcollection = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "DigitalSignature", true);
            X509Certificate2Collection fcollection2 = fcollection.Find(X509FindType.FindByKeyUsage, "KeyEncipherment", true);

            X509Certificate2Collection x509Certificate2Collection = fcollection2.Find(X509FindType.FindByExtension, "2.5.29.37", true);

            var list = x509Certificate2Collection.ToList();

            X509Certificate2Collection x509Certificate2Collection1 = new X509Certificate2Collection();

            foreach (var item in list)
            {
                var x509ExtensionEnumerator = item.Extensions.GetEnumerator();
                while (x509ExtensionEnumerator.MoveNext())
                {
                    var x509Extension = x509ExtensionEnumerator.Current;
                    if (x509Extension.Oid.Value == "2.5.29.37")
                    {
                        OidEnumerator oidEnumerator = ((X509EnhancedKeyUsageExtension)x509Extension).EnhancedKeyUsages.GetEnumerator();
                        while (oidEnumerator.MoveNext())
                        {
                            if (oidEnumerator.Current.Value == "1.3.6.1.5.5.7.3.2")
                            {
                                x509Certificate2Collection1.Add(item);
                            }
                        }
                    }
                }
            }

            store.Close();

            return x509Certificate2Collection1;
        }
    }
}
