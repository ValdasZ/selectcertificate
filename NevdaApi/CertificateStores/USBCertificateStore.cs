﻿using Security.Cryptography;
using Security.Cryptography.X509Certificates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Api
{
    public class USBCertificateStore : AbstractCertificateStore
    {
        protected override X509Certificate2Collection getCollection()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
            X509Certificate2Collection fcollectionValidDate = collection.Find(X509FindType.FindByTimeValid, DateTime.Now, true);
            X509Certificate2Collection fcollection = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "DigitalSignature", true);
            X509Certificate2Collection fcollection2 = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "KeyEncipherment", true);

            IList<X509Certificate2> list = fcollection.ToList();
            IList<X509Certificate2> list2 = fcollection2.ToList();
            IList<X509Certificate2> list3 = list.Where(x => !list2.Contains(x)).ToList().OrderBy(x => x.FriendlyName).ToList().Distinct().ToList();
            IList<X509Certificate2> list4 = new List<X509Certificate2>();

            foreach (var item in list3)
            {
                foreach (var ext in item.Extensions)
                {
                    var eku = ext as X509EnhancedKeyUsageExtension;
                    if (eku != null)
                    {
                        foreach (var oid in eku.EnhancedKeyUsages)
                        {
                            if (oid.FriendlyName == "Document Signing")
                            {
                                list4.Add(item);
                            }
                        }
                    }
                }
            }

            IList<X509Certificate2> list5 = new List<X509Certificate2>();

            foreach (var item in list4)
            {
                try
                {
                    var privateKey = item.PrivateKey;
                    list5.Add(item);
                }
                catch
                {

                }
            }

            X509Certificate2Collection cole = new X509Certificate2Collection(list5.ToArray());

            store.Close();

            return cole;
        }
    }
}
