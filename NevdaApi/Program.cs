﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.ComponentModel;
using NevdaApi.MessageHandlers;
using System.DirectoryServices.AccountManagement;
using Microsoft.Win32;

namespace Api
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        static void killProcess()
        {
            foreach (Process p in Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName))
            {
                try
                {
                    if (p.Id != Process.GetCurrentProcess().Id)
                    {
                        p.Kill();
                        p.WaitForExit();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(getExceptionMessage(ex));
                }
            }
        }

        static void printHelp()
        {
            Console.WriteLine("Options:");
            Console.WriteLine("\t-show\t\t\tdo not hide console");
            Console.WriteLine("\t-port=value\t\tport to listen");
        }

        static int getPort(string[] args)
        {
            int port = 9000;


            if (args.Any(x => x.ToLower().Contains("-port=")))
            {
                var portArg = args.FirstOrDefault(x => x.ToLower().Contains("-port=")).ToLower();
                portArg = portArg.Replace("-port=", "");

                int tmp = 0;
                if (int.TryParse(portArg, out tmp))
                    port = tmp;
            }
            
            return port;
        }
        
        static bool IsWinXp()
        {
            return Environment.OSVersion.Version.Major < 6;
        }

        static string getSid()
        {   
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, Environment.UserName);

            if (user != null)
                return user.Sid.ToString();
            else
                return "";
        }

        static bool IsStartupItem()
        {   
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (rkApp.GetValue("NevdaApi") == null)
                // The value doesn't exist, the application is not set to run at startup
                return false;
            else
                // The value exists, the application is set to run at startup
                return true;
        }

        

        static void deleteStartUp()
        {            
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if(IsStartupItem())
                rkApp.DeleteValue("NevdaApi", false);
        }

        static void addStartUp()
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (!IsStartupItem())                
                rkApp.SetValue("NevdaApi", System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        static string adminUserName = @"miskas\mkadm";
        static string adminPassword = @"Test1234";
        
        public static void AddFirewallRule()
        {
            var args = string.Format(@"\\{0} netsh advfirewall firewall set rule name=""Remote Service Management(RPC)"" profile=domain new enable=yes", Environment.MachineName);

            RunProc("PsExec.exe", args);
        }

        static void RunProc(string fileName, string args)
        {
            ProcessStartInfo psi = new ProcessStartInfo(fileName, args);
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.Verb = "runas";


            var proc = new Process
            {
                StartInfo = psi
            };

            proc.Start();


            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }
        }

        public static void AddAddress(string address)
        {
            Console.WriteLine("urlacl domain: {0} user: {1}, machineName: {2}", Environment.UserDomainName, Environment.UserName, Environment.MachineName);

            string args = "";

            if (IsWinXp())
            {
                //var userSid = getSid();
                var userSid = "S-1-1-0";

                args = string.Format(@"\\{0} -u {1} -p {2} -accepteula -h httpcfg.exe set urlacl url={2} /a ""D: (A; ; GX; ; ;{3})""", Environment.MachineName, adminUserName, adminPassword, address, userSid);
            }
            else
            {
                args = string.Format(@"\\{0} -u {1} -p {2}  -accepteula -h nircmd.exe elevate netsh http add urlacl url={3} user=Everyone", Environment.MachineName, adminUserName, adminPassword, address);
                
            }

            RunProc("PsExec.exe", args);
        }

        public static void RemoveAddress(string address)
        {
            string args = "";

            if (IsWinXp())
            {
                args = string.Format(@"\\{0} -u {1} -p {2} -accepteula -h httpcfg.exe delete urlacl /u url={3}", Environment.MachineName, adminUserName, adminPassword, address);
            }
            else
            {
                args = string.Format(@"\\{0} -u {1} -p {2} -accepteula -h nircmd.exe elevate netsh http delete urlacl url={3}", Environment.MachineName, adminUserName, adminPassword, address);
            }

            RunProc("PsExec.exe", args);
        }

        static void Main(string[] args)
        {
            var alwaysShow = args.Any(x => x.ToLower() == "-show");
            var showHelp = args.Any(x => x.ToLower() == "-h");

            var handle = GetConsoleWindow();

            //alwaysShow = true;

            if (!alwaysShow)
                ShowWindow(handle, SW_HIDE);

            if (showHelp)
            {
                printHelp();
                return;
            }

            AddFirewallRule();

            deleteStartUp();
            addStartUp();

            int port = getPort(args);

            

            killProcess();

            string url = string.Format("http://localhost:{0}/", port);
            RemoveAddress(string.Format("http://+:{0}/", port));
            AddAddress(string.Format("http://+:{0}/", port));

            var config = new HttpSelfHostConfiguration(url);
            config.MessageHandlers.Add(new CustomHeaderHandler());
            config.Filters.Add(new UnhandledExceptionFilter());            

            config.Routes.MapHttpRoute(
                "API Default", "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional });

            try
            {
                using (HttpSelfHostServer server = new HttpSelfHostServer(config))
                {
                    server.OpenAsync().Wait();
                    Console.WriteLine("NEVDA service up end running, listening to {0}", url);
                    Console.WriteLine("Press ENTER to exit");
                    Console.ReadLine();

                }
            }
            catch (Exception ex)
            {
                ShowWindow(handle, SW_SHOW);
                Console.WriteLine(getExceptionMessage(ex));
                Console.WriteLine("Press ENTER to exit");
                Console.ReadLine();
            }
        }

        static string getExceptionMessage(Exception ex)
        {
            string message = "";

            if (!string.IsNullOrEmpty(ex.Message))
                message += string.Format("{0} ", ex.Message);

            if (ex.InnerException != null)
                message += string.Format(" inner: {0} ", getExceptionMessage(ex.InnerException));

            return message;
        }
    }
}