﻿using NevdaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Api
{   
    public class AllCertController : ApiController
    {
        AbstractCertificateStore certificateStore { get; set; }

        public AllCertController()
        {   
            certificateStore = new AllCertificateStore();
        }        

        [HttpGet]
        public GetCertificateModel Get()
        {   
            return certificateStore.GetCertModel();
        }

        [HttpPost]
        public string Sign(SignModel model)
        {
            var signedData = certificateStore.Sign(model);
            return signedData;
        }
    }
}