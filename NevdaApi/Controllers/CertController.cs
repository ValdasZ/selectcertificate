﻿using NevdaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Api
{   
    public class CertController : ApiController
    {
        AbstractCertificateStore certificateStore { get; set; }

        public CertController()
        {
            //certificateStore = new USBCertificateStore();
            certificateStore = new WindowsCertificateStore();
        }

        [HttpGet]
        public GetCertificateModel Get()
        {   
            return certificateStore.GetCertModel();
        }

        [HttpPost]
        public string Sign(SignModel model)
        {
            var signedData = certificateStore.Sign(model);
            return signedData;
        }

        [HttpGet]
        public string Test()
        {

            var key = Get();
            
            var hash = GetHashString("testdata");
            
            var model = new SignModel()
            {
                algorithm = key != null ? key.Algorithm : "sha256",
                certificate = key != null ? key.Certificate : null,
                dtbs = hash
            };

            Sign(model);
            

            return null;
        }

        public byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}