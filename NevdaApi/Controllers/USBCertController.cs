﻿using NevdaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Api
{   
    public class USBCertController : ApiController
    {
        AbstractCertificateStore certificateStore { get; set; }

        public USBCertController()
        {
            certificateStore = new USBCertificateStore();
        }

        [HttpGet]
        public GetCertificateModel Get()
        {   
            return certificateStore.GetCertModel();
        }

        [HttpPost]
        public string Sign(SignModel model)
        {
            var signedData = certificateStore.Sign(model);
            return signedData;
        }
    }
}