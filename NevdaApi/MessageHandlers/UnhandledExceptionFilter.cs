﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Filters;

namespace NevdaApi.MessageHandlers
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {   
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;

            var klaiduSarasas = GetErrors(context.Exception);
            HttpError httpError = new HttpError();
           
            httpError["RodytiKlaidas"] = true;
            httpError["Klaidos"] = klaiduSarasas;

            context.Response = context.Request.CreateErrorResponse(statusCode, httpError);
            base.OnException(context);
        }

        IList<OperacijosRezultatoPranesimas> GetErrors(Exception ex)
        {
            IList<OperacijosRezultatoPranesimas> errors = new List<OperacijosRezultatoPranesimas>();

            if (!string.IsNullOrEmpty(ex.Message))
                errors.Add(new OperacijosRezultatoPranesimas(ex.Message));

            if (ex.InnerException != null)
                errors = errors.Concat(GetErrors(ex.InnerException)).ToList();

            return errors;
        }
    }

    public class OperacijosRezultatoPranesimas
    {
        public string Tekstas { set; get; }

        public OperacijosRezultatoPranesimas(string tekstas)
        {
            Tekstas = tekstas;
        }
    }
}
