﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NevdaApi.MessageHandlers
{
    public class CustomHeaderHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken)
                .ContinueWith((task) =>
                {
                    if (request.Method == HttpMethod.Options)
                    {
                        var emptyResponse = new HttpResponseMessage(HttpStatusCode.OK);
                        emptyResponse.Headers.Add("Access-Control-Allow-Origin", "*");
                        emptyResponse.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
                        emptyResponse.Headers.Add("Access-Control-Allow-Credentials", "true");
                        emptyResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Authorization");

                        return emptyResponse;
                    }
                    else
                    {
                        task.Result.Headers.Add("Access-Control-Allow-Origin", "*");
                        task.Result.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
                        task.Result.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Authorization");

                        return task.Result;
                    }
                });
        }
    }
}
