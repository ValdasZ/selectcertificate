﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Api
{
    public class SignModel
    {
        public string dtbs { get; set; }
        public string algorithm { get; set; }
        public string certificate { get; set; }
    }
}
