﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NevdaApi.Models
{
    public class GetCertificateModel
    {
        public string Certificate { get; set; }
        public string Algorithm { get; set; }
    }
}
