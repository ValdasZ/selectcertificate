﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Security;

namespace SelectCertificate
{
    public static class CertSelectUSB
    {
        public static void Select()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            IList<X509Certificate2> list = new List<X509Certificate2>();
            IList<X509Certificate2> list2 = new List<X509Certificate2>();

            X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;

            X509Certificate2Collection fcollectionValidDate = collection.Find(X509FindType.FindByTimeValid, DateTime.Now, true);            

            X509Certificate2Collection fcollection = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "DigitalSignature", true);
            X509Certificate2Collection fcollection2 = fcollectionValidDate.Find(X509FindType.FindByKeyUsage, "KeyEncipherment", true);

            

            var items = fcollection.GetEnumerator();            
            var items2 = fcollection2.GetEnumerator();          

            while (items.MoveNext())
            {
                list.Add(items.Current);
            }            

            while (items2.MoveNext())
            {
                list2.Add(items2.Current);
            }


            var list3 = list.Where(x => !list2.Contains(x)).ToList().OrderBy(x => x.FriendlyName).ToList().Distinct().ToList();
            IList<X509Certificate2> list4 = new List<X509Certificate2>();


            foreach (var item in list3)
            {
                foreach (var ext in item.Extensions)
                {
                    var eku = ext as X509EnhancedKeyUsageExtension;
                    if (eku != null)
                    {
                        foreach (var oid in eku.EnhancedKeyUsages)
                        {
                            if (oid.FriendlyName == "Document Signing")
                            {
                                list4.Add(item);
                            }
                        }
                    }
                }
                
            }

            IList<X509Certificate2> list5 = new List<X509Certificate2>();

            foreach (var item in list4)
            {
                try
                {
                    var privateKey = item.PrivateKey;
                    list5.Add(item);
                }
                catch
                {

                }
            }


            X509Certificate2Collection cole = new X509Certificate2Collection(list5.ToArray());


            X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(cole, "Dokumento pasirašymas", "Pasirinkite sertifikatą kurį norėsite naudoti", X509SelectionFlag.SingleSelection);
            Console.WriteLine("Number of certificates: {0}{1}", scollection.Count, Environment.NewLine);

            foreach (X509Certificate2 x509 in scollection)
            {
                try
                {
                    byte[] rawdata = x509.RawData;
                    Console.WriteLine("Content Type: {0}{1}", X509Certificate2.GetCertContentType(rawdata), Environment.NewLine);
                    Console.WriteLine("Friendly Name: {0}{1}", x509.FriendlyName, Environment.NewLine);
                    Console.WriteLine("Certificate Verified?: {0}{1}", x509.Verify(), Environment.NewLine);
                    Console.WriteLine("Simple Name: {0}{1}", x509.GetNameInfo(X509NameType.SimpleName, true), Environment.NewLine);
                    Console.WriteLine("Signature Algorithm: {0}{1}", x509.SignatureAlgorithm.FriendlyName, Environment.NewLine);
                    Console.WriteLine("Private Key: {0}{1}", x509.PrivateKey.ToXmlString(false), Environment.NewLine);
                    Console.WriteLine("Public Key: {0}{1}", x509.PublicKey.Key.ToXmlString(false), Environment.NewLine);
                    Console.WriteLine("Certificate Archived?: {0}{1}", x509.Archived, Environment.NewLine);
                    Console.WriteLine("Length of Raw Data: {0}{1}", x509.RawData.Length, Environment.NewLine);

                    var csp = (RSACryptoServiceProvider)x509.PrivateKey;

                    SHA1Managed sha1 = new SHA1Managed();
                    UnicodeEncoding encoding = new UnicodeEncoding();

                    byte[] data = encoding.GetBytes("vz");

                    byte[] hash = sha1.ComputeHash(data);


                    // Sign the hash
                    var signedHash = csp.SignHash(hash, CryptoConfig.MapNameToOID("SHA1"));
                    //X509Certificate2UI.DisplayCertificate(x509);
                    x509.Reset();

                    Console.ReadLine();

                }
                catch (CryptographicException)
                {
                    Console.WriteLine("Information could not be written out for this certificate.");
                }
            }
            store.Close();
        }
    }
}
