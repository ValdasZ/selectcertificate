﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SelectCertificate
{
    public enum CertSelectionMode
    {
        ShowAll,
        QCOnly,
        AuthOnly
    }

    public static class CertBaranenko
    {
        public static X509Certificate2 GetCertificate(string issuerList = null, CertSelectionMode certSel = 0)
        {
            X509Certificate2 current;
            X509Extension x509Extension;
            X509Certificate2Enumerator enumerator;
            X509ExtensionEnumerator x509ExtensionEnumerator;
            List<string> strs;
            X509Certificate2 item = null;
            object obj = null;
            X509Certificate2Collection x509Certificate2Collection = null;
            X509Certificate2Collection x509Certificate2Collection1 = null;
            if (string.IsNullOrEmpty(issuerList))
            {
                strs = null;
            }
            else
            {
                char[] chrArray = new char[] { '|' };
                strs = new List<string>(issuerList.Split(chrArray));
            }
            List<string> strs1 = strs;
            X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            try
            {
                x509Store.Open(OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certificates = x509Store.Certificates;
                if (certSel == CertSelectionMode.AuthOnly)
                {
                    x509Certificate2Collection = certificates.Find(X509FindType.FindByExtension, "2.5.29.37", true);
                    x509Certificate2Collection1 = new X509Certificate2Collection();
                    enumerator = x509Certificate2Collection.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        current = enumerator.Current;
                        x509ExtensionEnumerator = current.Extensions.GetEnumerator();
                        while (x509ExtensionEnumerator.MoveNext())
                        {
                            x509Extension = x509ExtensionEnumerator.Current;
                            if (x509Extension.Oid.Value == "2.5.29.37")
                            {
                                OidEnumerator oidEnumerator = ((X509EnhancedKeyUsageExtension)x509Extension).EnhancedKeyUsages.GetEnumerator();
                                while (oidEnumerator.MoveNext())
                                {
                                    if (oidEnumerator.Current.Value == "1.3.6.1.5.5.7.3.2")
                                    {
                                        x509Certificate2Collection1.Add(current);
                                    }
                                }
                            }
                        }
                    }
                    certificates = x509Certificate2Collection1;
                }
                else if (certSel == CertSelectionMode.QCOnly)
                {
                    obj = X509KeyUsageFlags.NonRepudiation | X509KeyUsageFlags.DigitalSignature;
                    x509Certificate2Collection = certificates.Find(X509FindType.FindByKeyUsage, obj, true);
                    x509Certificate2Collection1 = new X509Certificate2Collection();
                    enumerator = x509Certificate2Collection.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        current = enumerator.Current;
                        x509ExtensionEnumerator = current.Extensions.GetEnumerator();
                        while (x509ExtensionEnumerator.MoveNext())
                        {
                            x509Extension = x509ExtensionEnumerator.Current;
                            if ((!(x509Extension.Oid.Value == "2.5.29.15") || !x509Extension.Critical ? false : ((X509KeyUsageExtension)x509Extension).KeyUsages.HasFlag((X509KeyUsageFlags)obj)))
                            {
                                x509Certificate2Collection1.Add(current);
                            }
                        }
                    }
                    certificates = x509Certificate2Collection1;
                }
                if ((strs1 == null ? false : strs1.Count > 0))
                {
                    x509Certificate2Collection1 = new X509Certificate2Collection();
                    for (int i = 0; i < strs1.Count; i++)
                    {
                        if (strs1.Count - 1 != i)
                        {
                            if (!(strs1[i + 1].ToString() == "false" ? false : !(strs1[i + 1].ToString() == "False")))
                            {
                                certificates = x509Store.Certificates;
                                x509Certificate2Collection = certificates.Find(X509FindType.FindByIssuerDistinguishedName, strs1[i], Convert.ToBoolean(strs1[i + 1]));
                                i++;
                                if (x509Certificate2Collection != null)
                                {
                                    x509Certificate2Collection1.AddRange(x509Certificate2Collection);
                                }
                                x509Certificate2Collection = null;
                            }
                            else if ((strs1[i + 1].ToString() == "true" ? false : !(strs1[i + 1].ToString() == "True")))
                            {
                                x509Certificate2Collection = certificates.Find(X509FindType.FindByIssuerDistinguishedName, strs1[i], true);
                                if (x509Certificate2Collection != null)
                                {
                                    x509Certificate2Collection1.AddRange(x509Certificate2Collection);
                                }
                                x509Certificate2Collection = null;
                            }
                            else
                            {
                                x509Certificate2Collection = certificates.Find(X509FindType.FindByIssuerDistinguishedName, strs1[i], true);
                                i++;
                                if (x509Certificate2Collection != null)
                                {
                                    x509Certificate2Collection1.AddRange(x509Certificate2Collection);
                                }
                                x509Certificate2Collection = null;
                            }
                        }
                    }
                    certificates = x509Certificate2Collection1;
                }
                certificates = X509Certificate2UI.SelectFromCollection(certificates, "Dokumento pasirašymas", "Pasirinkite sertifikatą kurį norėsite naudoti", X509SelectionFlag.SingleSelection);
                if (certificates.Count == 1)
                {
                    item = certificates[0];
                }
                else if (certificates.Count > 1)
                {
                    throw new Exception("Pasirinkta daugiau negu vienas sertifikatas");
                }
            }
            finally
            {
                x509Store.Close();
            }
            return item;
        }
    }
}
