﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.DirectoryServices;
using System.IO;

namespace SelectCertificate
{
    public static class CertSelectAD
    {


        public static void Select()
        {
            DirectoryEntry de = new DirectoryEntry("LDAP://AZUOLAS");  //Where ##### is the name of your AD server
            DirectorySearcher dsearch = new DirectorySearcher(de);
            dsearch.Filter = "(cn=*)";
            SearchResultCollection rc = dsearch.FindAll();
            IList<X509Certificate2> stt = new List<X509Certificate2>();

            foreach (SearchResult r in rc)
            {
                if (r.Properties.Contains("userCertificate"))
                {
                    var certs = r.Properties["userCertificate"];

                    foreach (var item in certs)
                    {
                        Byte[] b = (Byte[])item;
                        X509Certificate2 cert1 = new X509Certificate2(b);
                        var key = cert1.PrivateKey;
                        stt.Add(cert1);
                    }
                }
            }

            

            for (int i = 0; i < stt.Count; i++)
			{
                var cert1 = stt[i];
            
                if (cert1.Subject.Contains("Zaramba"))
                {
                    byte[] certData = cert1.Export(X509ContentType.Pfx);
                    X509Certificate2 cert2 = new X509Certificate2(certData);
                    File.WriteAllBytes(string.Format("MyCert{0}.pfx", i), certData);
                }



                
            }



            int a = 0;
        }

        //public static void Select()
        //{
        //    X509Store store = new X509Store(StoreName.AuthRoot);
        //    store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
        //    X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;

        //    var list = collection.ToList();
        //}
    }
}
